var pmx = require('pmx').init({
  http          : true, // HTTP routes logging (default: true)
  ignore_routes : [/socket\.io/, /notFound/], // Ignore http routes with this pattern (Default: [])
  errors        : true, // Exceptions logging (default: true)
  custom_probes : true, // Auto expose JS Loop Latency and HTTP req/s as custom metrics
  network       : true, // Network monitoring at the application level
  ports         : true  // Shows which ports your app is listening on (default: false)
});


var express = require('express'); 
var bodyParser = require('body-parser'); 
var app = express(); 
var https = require('https'); 
var mongo = require('mongodb'); 
var assert = require('assert'); 
var request = require('request'); 
const querystring = require('querystring'); 
var url = require('url'); 
var crypto = require('crypto'); 

var fs = require('fs'); 
var util = require('util'); 
var logFile = fs.createWriteStream('log_beta.txt', { flags: 'a' }); 
var logStdout = process.stdout; 

console.log = function () { 
  var d = new Date(); 
  logFile.write(d + ' | ' + util.format.apply(null, arguments) + "\n"); 
  logStdout.write(d + ' | ' + util.format.apply(null, arguments) + "\n"); 
} 
console.error = console.log; 

app.use(bodyParser.json()); // support json encoded bodies 
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies 

var config = {}; 
config.database_name = 'shukran_static'; 
config.shukran_host = 'beta.shukranrewards.com'; 
config.mportal_host = 'beta.mportal.shukranrewards.com';


var mongourl = 'mongodb://localhost:27017/'+config.database_name; 

app.all('/mobile/*', function (req, res) { 
  var cursorArray = []; 
  var url_parts = url.parse(req.url, true); 
  console.log('Request url : '+url_parts.path); 
  console.log('Request method : '+req.method); 
  if(req.method == 'POST') { 
    url_parts.path = url_parts.path+'?'+querystring.stringify(req.body); 
  } 

  if ((url_parts.path.indexOf('timestamp') > -1) && (url_parts.path.indexOf('timestamp=0') === -1)){ //timestamp  exists and not equal to 0
   
    var pathasparts = url_parts.path.split("&timestamp");
    url_parts.path = pathasparts[0] + pathasparts[1].substring(11,pathasparts[1].length);
  }
	
if ((url_parts.path.indexOf('&ts') > -1) && (url_parts.path.indexOf('&ts=0') === -1)){ //timestamp  exists and not equal to 0
   
    var pathasparts = url_parts.path.split("&ts=");
    url_parts.path = pathasparts[0] + pathasparts[1].substring(11,pathasparts[1].length);
  }
  var pathaskey = url_parts.path; 
  var endpoint = 'collection'+url_parts.pathname.replace(/\//g,"_"); 
  mongo.connect(mongourl,function(err,db){ 
    assert.equal(null,err); 
    db.collection(endpoint).find({'_id':pathaskey}).count(function(err, count) { 
      console.log('No of Record : '+count); 
      if( count == 0) { 
        if(req.method == 'GET') { 
          getApis({'search':url_parts},function(rex){ 
            res.send(rex); 
          }); 
        } else if(req.method == 'POST') { 
          postApis({'search':url_parts,'postdata':req.body},function(rez){ 
            res.send(rez); 
          }); 
        } 
      } else { 
        cursor = db.collection(endpoint).find({'_id':pathaskey}); 
        cursor.forEach(function(doc,err){ 
          assert.equal(null,err); 
          cursorArray.push(doc.data); 
        },function(){ 
          db.close(); 
          console.log("Send Response"); 
          res.send(cursorArray[0]); 
        }); 
      } 

    }); 
  }); 
}); 

app.get('/clear_collections', function(req, res){ 
  var url_parts = url.parse(req.url, true); 
  var query = url_parts.query; 
  if(query.token == '34514ff4871119d14f4339b5a43354b9') { 
    mongo.connect(mongourl,function(err,db){ 
      db.listCollections().forEach(function(collInfos) { 
        console.log('Deleting collection : '+collInfos.name); 
        db.collection(collInfos.name).drop(); 
      }); 
    }); 
    res.send('done'); 
  } else { 
    res.send('invalid request'); 
  } 
}); 

app.get('/clear_documents', function(req, res){ 
  var url_parts = url.parse(req.url, true); 
  var query = url_parts.query; 
  if(query.token == '34514ff4871119d14f4339b5a43354b9') { 
    mongo.connect(mongourl,function(err,db){ 
      if(query.hasOwnProperty('collection')) { 
        db.collection(query.collection).remove({}); 
      } 
    }); 
    res.send('done'); 
  } else { 
    res.send('invalid request'); 
  } 
}); 

var getApis = function(data, callback) { 
  var param = data.search.path; 
  console.log(data.search); 
  request({ 
    url: 'https://'+config.shukran_host+data.search.pathname, 
    qs: data.search.query, 
    method: 'GET', 
  }, function (error, response, body) { 
    if (error) { 
      console.log('Error sending message: ', error); 
    } else if (response.body.error) { 
      console.log('Error: ', response.body); 
    } 

    var jsonValue = JSON.parse(response.body); 
    var test = {}; 
    test['_id'] = param; 
    test['data'] = jsonValue; 
    var collectionVal = 'collection'+data.search.pathname.replace(/\//g,"_"); 
    mongo.connect(mongourl,function(err,db){ 
      db.collection(collectionVal).insertOne(test, function(err, result) { 
        console.log("Inserted a document into the " +collectionVal+ " collection."); 
        db.close(); 
        callback(jsonValue); 
      }); 
    }); 

  }); 
}; 

var postApis = function(data, callback) { 
  var param = data.search.path; 
  console.log(data.search); 
  request({ 
    url: 'https://'+config.shukran_host+data.search.pathname, 
    headers: {'content-type':'application/json'}, 
    json: data.postdata, 
    method: 'POST', 
  }, function (error, response, body) { 
    if (error) { 
      console.log('Error sending message: ', error); 
    } else if (response.body.error) { 
      console.log('Error: ', response.body); 
    } 
    var jsonValue = response.body; 
    var test = {}; 
    test['_id'] = param; 
    test['data'] = jsonValue; 
    var collectionVal = 'collection'+data.search.pathname.replace(/\//g,"_"); 
    mongo.connect(mongourl,function(err,db){ 
      db.collection(collectionVal).insertOne(test, function(err, result) { 
        console.log("Inserted a document into the " +collectionVal+ " collection."); 
        db.close(); 
        callback(jsonValue); 
      }); 
    }); 

  }); 
}; 

var server = app.listen(3000, function () { 
  var host = server.address().address 
  var port = server.address().port 
  console.log("Node Server Started On Host : "+host+" Port : "+port); 
}) 
